/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author senzo
 */
public class DBConnection {
    private Connection connection;
    private Statement statement;
    private final String CONNECTION_STRING = "jdbc:ucanaccess://Database1.accdb";
    
    private void ObtainConnection(){
        try{
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
            connection = DriverManager.getConnection(CONNECTION_STRING);
            statement = connection.createStatement();
        }
        catch(Exception ex){
            System.err.println("Sorry there was an error trying to obtain connection.\nError Message : " + ex.getMessage());
        }
    }
    
    private void DisposeConnection(){
        try{
            connection.close();
        }
        catch(Exception ex){
            System.err.println("Sorry there was an error trying to dispose connection.\nError Message : " + ex.getMessage());
        }
    }
    
    public int ModifyDatabase(String sql){
        try{
            ObtainConnection();
            int status = statement.executeUpdate(sql);
            return status;
        }
        catch(Exception ex){
            System.err.println("Sorry there was an error tryig to modify database.\nError Message : " + ex.getMessage());
            return 0;
        }
        finally{
            DisposeConnection();
        }
    }
    
    public ResultSet QueryDatabase(String sql){
        try{
            ObtainConnection();
            ResultSet resultSet = statement.executeQuery(sql);
            return resultSet;
        }
        catch(Exception ex){
            System.err.println("Sorry there was an error tryig to query database.\nError Message : " + ex.getMessage());
            return null;
        }
        finally{
            DisposeConnection();
        }
    }
}
